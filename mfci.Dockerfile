FROM registry.gitlab.com/talmer/ansible:debian-slim

LABEL "com.example.vendor"="Talmer"

RUN apt-get update -y && apt-get upgrade -y && \
    DEBIAN_FRONTEND=noninteractive             \
    apt-get install -y                         \                            
    unzip git                               && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*  /tmp/*

RUN curl -sSL https://gitlab.com/talmer/edini/-/jobs/130150209/artifacts/download -o ./edini.zip  && \
    unzip -o ./edini.zip edini -d /usr/local/bin/ && \
    chmod +x /usr/local/bin/edini


